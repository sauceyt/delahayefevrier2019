using System;
using Xunit;

using chainedecaracteresproj;

namespace chainedecaracterestest
{
    public class UniTest1
    {
        [Fact]
        public void TestMethod1()
        {
            Assert.Equal(true, chainedecaracteresproj.Program.Majuscule("Les framboises sont perchées sur le tabouret de mon grand-pere."));
            Assert.Equal(false, chainedecaracteresproj.Program.Majuscule("les framboises sont perchées sur le tabouret de mon grand-pere."));
            Assert.Equal(false, chainedecaracteresproj.Program.Majuscule("les Framboises Sont Perchées Sur Le Tabouret De Mon Grand-Pere."));
            Assert.Equal(true, chainedecaracteresproj.Program.Majuscule("LES FRAMBOISES SONT PERCHEES SUR LE TABOURET DE MON GRAND-PERE."));
            Assert.Equal(false, chainedecaracteresproj.Program.Majuscule("5Les framboises sont perchées sur le tabouret de mon grand-pere."));
            Assert.Equal(false, chainedecaracteresproj.Program.Majuscule("@Les framboises sont perchées sur le tabouret de mon grand-pere."));
            Assert.Equal(true, chainedecaracteresproj.Program.Majuscule("A231(."));
            Assert.Equal(false, chainedecaracteresproj.Program.Majuscule(" "));
            Assert.Equal(false, chainedecaracteresproj.Program.Majuscule(""));


            Assert.Equal(true, chainedecaracteresproj.Program.Point("Les framboises sont perchées sur le tabouret de mon grand-pere."));
            Assert.Equal(false, chainedecaracteresproj.Program.Point("Les framboises sont perchées sur le tabouret de mon grand-pere"));
            Assert.Equal(false, chainedecaracteresproj.Program.Point("Les. framboises. son.t perchées. sur. le. tabouret. de. mon. grand-pere"));
            Assert.Equal(false, chainedecaracteresproj.Program.Point("Les framboises sont perchées sur le tabouret de mon grand-pere,"));
            Assert.Equal(true, chainedecaracteresproj.Program.Point("Les framboises sont perchées sur le tabouret de mon grand-pere!"));
            Assert.Equal(true, chainedecaracteresproj.Program.Point("Les framboises sont perchées sur le tabouret de mon grand-pere?"));
            Assert.Equal(true, chainedecaracteresproj.Program.Point("Les framboises sont perchées sur le tabouret de mon grand-pere;"));
            Assert.Equal(true, chainedecaracteresproj.Program.Point("Les framboises sont perchées sur le tabouret de mon grand-pere..."));
            Assert.Equal(true, chainedecaracteresproj.Program.Point("."));
            Assert.Equal(true, chainedecaracteresproj.Program.Point("?,:!."));
            Assert.Equal(true, chainedecaracteresproj.Program.Point("546."));
            Assert.Equal(false, chainedecaracteresproj.Program.Point("Les framboises sont perchées sur le tabouret de mon grand-pere.5"));
            Assert.Equal(false, chainedecaracteresproj.Program.Majuscule(" "));
            Assert.Equal(false, chainedecaracteresproj.Program.Majuscule(""));
        }
    }
}

