﻿using System;

namespace chainedecaracteresproj
{
    public class Program
    {

        public static bool Majuscule(string Phrase)
        {
            if (Phrase.Length == 0)
            {
                return false;
            }
            char FirstLetter = Phrase[0];
            if (FirstLetter >= 'A' && FirstLetter <= 'Z')
            {
                return true;
            }
            return false;
        }

        public static bool Point(string Phrase)
        {
            int NbCar = Phrase.Length;
            if (NbCar == 0)
            {
                return false;
            }
            char LastLetter = Phrase[NbCar - 1];
            if (LastLetter == '.' || LastLetter == '?' || LastLetter == '!' || LastLetter == ';')
            {
                return true;
            }
            return false;
        }

        static void Main(string[] args)
        {
            string Saisie;
            Console.WriteLine("Saisissez une phrase : ");
            Saisie = Console.ReadLine();
            Console.WriteLine("Vous avez saisi : {0}", Saisie);
            if (Majuscule(Saisie))
            {
                Console.WriteLine("Cette phrase commence par une majuscule.");
            }
            if (Point(Saisie))
            {
                Console.WriteLine("Cette phrase se termine par un point.");
            }
        }
    }
}
