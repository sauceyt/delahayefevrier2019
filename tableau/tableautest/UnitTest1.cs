using System;
using Xunit;

using tableauproj;

namespace tableautest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            Assert.AreEqual(42, TableauConsole.Program.ConvertNumeric("42"));
            Assert.AreEqual(984, TableauConsole.Program.ConvertNumeric("984"));
            Assert.AreEqual(0, TableauConsole.Program.ConvertNumeric("4ml2"));
            Assert.AreEqual(42, TableauConsole.Program.ConvertNumeric("42"));
            Assert.AreEqual(0, TableauConsole.Program.ConvertNumeric("0"));
            int[] ValeursSaisies = {3,2,7,85,98,74,56,4,0,4};
            Assert.AreEqual(3, TableauConsole.Program.RechercheVal(ValeursSaisies,7));
            Assert.AreEqual(0, TableauConsole.Program.RechercheVal(ValeursSaisies,5));
            Assert.AreEqual(9, TableauConsole.Program.RechercheVal(ValeursSaisies,0));
            Assert.AreEqual(8, TableauConsole.Program.RechercheVal(ValeursSaisies,4));
        }
    }
}
