﻿using System;

namespace tableauproj
{
    public class Program
    {
        public static int ConvertNumeric(string Val)
        {
            int ConvertVal = 0;
            if (int.TryParse(Val, out ConvertVal))
            {
                return ConvertVal;
            }
            return 0;
        }

        public static int RechercheVal(int[] TabVal, int Val)
        {
            int Compteur = 0, ValPos = 0;
            do
            {
                if (TabVal[Compteur] == Val)
                {
                    ValPos = Compteur + 1;
                }
                Compteur++;
            } while (Compteur < 10 && ValPos == 0);
            return ValPos;
        }

        static void Main(string[] args)
        {
            int[] ValeursSaisies = new int[10];
            string Saisie;
            Console.WriteLine("Saisissez 10 valeurs non nulles : ");
            for (int Boucle = 0; Boucle < 10; Boucle++)
            {
                do
                {
                    Console.Write("{0} : ", Boucle + 1);
                    Saisie = Console.ReadLine();
                } while (ConvertNumeric(Saisie) == 0);
                ValeursSaisies[Boucle] = ConvertNumeric(Saisie);
            }
            Console.WriteLine("Saisissez une valeur : ");
            Saisie = Console.ReadLine();
            if (RechercheVal(ValeursSaisies, ConvertNumeric(Saisie)) == 0)
            {
                Console.WriteLine("Aucune valeur correspondante trouvée !");
            }
            else
            {
                Console.WriteLine("{0} est la {1}ème valeur saisie.", Saisie, RechercheVal(ValeursSaisies, ConvertNumeric(Saisie)));
            }
            Console.Read();
        }
    }
}